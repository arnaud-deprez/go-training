package concurrency

import (
	"fmt"
	"testing"

	"golang.org/x/tour/tree"
)

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
	var walker func(t *tree.Tree, ch chan int)
	walker = func(t *tree.Tree, ch chan int) {
		for t != nil {
			walker(t.Left, ch)
			ch <- t.Value
			t = t.Right
		}
	}
	walker(t, ch)
	close(ch)
}

func TestWalk(*testing.T) {
	ch := make(chan int)
	t := tree.New(1)
	go Walk(t, ch)
	for i := 0; i < 10; i++ {
		fmt.Println(<-ch)
	}
	fmt.Println(t)
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
	fmt.Println("Comparing ", t1, " and ", t2, "...")
	ch1, ch2 := make(chan int), make(chan int)
	go Walk(t1, ch1)
	go Walk(t2, ch2)
	for v1 := range ch1 {
		v2, ok := <-ch2
		if !ok || v1 != v2 {
			fmt.Printf("v1(%v) != v2(%v)\n", v1, v2)
			return false
		}
	}
	_, ok := <-ch2
	return !ok
}

func TestSame(*testing.T) {
	fmt.Println("Are these trees equal ? ", Same(tree.New(1), tree.New(1)))
	fmt.Println("Are these trees equal ? ", Same(tree.New(1), tree.New(2)))
}
