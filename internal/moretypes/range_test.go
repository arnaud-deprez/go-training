package moretypes

import (
	"fmt"
	"testing"
)

func TestRange(*testing.T) {
	var pow = []int{1, 2, 4, 8, 16, 32, 64, 128}

	// range form of for loop: iterate over a slice or a map
	// range return index and value
	for i, v := range pow {
		fmt.Printf("2**%d = %d\n", i, v)
	}
}

func TestRangeWithNoIndexOrValue(*testing.T) {
	pow := make([]int, 10)
	// only index
	for i := range pow {
		pow[i] = 1 << uint(i) // == 2**i
	}
	// index is discarded _
	for _, value := range pow {
		fmt.Printf("%d\n", value)
	}
}
