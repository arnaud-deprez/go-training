package moretypes

import (
	"fmt"
	"math"
	"testing"
)

func TestFunctionValues(*testing.T) {
	compute := func(fn func(float64, float64) float64) float64 {
		return fn(3, 4)
	}
	hypot := func(x, y float64) float64 {
		return math.Sqrt(x*x + y*y)
	}
	fmt.Println(hypot(5, 12))

	fmt.Println(compute(hypot))
	fmt.Println(compute(math.Pow))
}

func TestFunctionClosures(*testing.T) {
	adder := func() func(int) int {
		sum := 0
		// the closure is bound to the var sum
		return func(x int) int {
			sum += x
			return sum
		}
	}
	// each closure is bound to its own sum variable
	pos, neg := adder(), adder()
	for i := 0; i < 10; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}
}

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	a, b := 0, 1
	return func() int {
		a, b = b, a+b
		return a
	}
}
func TestFibonacci(*testing.T) {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
