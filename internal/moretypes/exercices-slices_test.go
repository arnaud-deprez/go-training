package moretypes

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

var dirName string

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func TestMain(m *testing.M) {
	er1 := os.MkdirAll("build", os.ModePerm)
	check(er1)
	dir, er2 := ioutil.TempDir("build", "img")
	check(er2)
	dirName = dir
	os.Exit(m.Run())
}

func BlackPic(dx, dy int) [][]uint8 {
	result := make([][]uint8, dy)
	for y := range result {
		result[y] = make([]uint8, dx)
	}
	return result
}

func MedianPic(dx, dy int) [][]uint8 {
	result := make([][]uint8, dy)
	for y := range result {
		result[y] = make([]uint8, dx)
		for x := range result[y] {
			result[y][x] = uint8((x + y) / 2)
		}
	}
	return result
}

func Mosaic1Pic(dx, dy int) [][]uint8 {
	result := make([][]uint8, dy)
	for y := range result {
		result[y] = make([]uint8, dx)
		for x := range result[y] {
			result[y][x] = uint8(x * y)
		}
	}
	return result
}

func Mosaic2Pic(dx, dy int) [][]uint8 {
	result := make([][]uint8, dy)
	for y := range result {
		result[y] = make([]uint8, dx)
		for x := range result[y] {
			result[y][x] = uint8(x ^ y)
		}
	}
	return result
}

func saveImage(name string, buf *bytes.Buffer) {
	fmt.Printf("Write %s in %s\n", name, dirName)
	ioutil.WriteFile(fmt.Sprintf("%s/%s", dirName, name), buf.Bytes(), 0644)
}

func TestBlackPic(*testing.T) {
	saveImage("black.png", Show(BlackPic))
}

func TestMedianPic(*testing.T) {
	saveImage("median.png", Show(MedianPic))
}

func TestMosaic1Pic(*testing.T) {
	saveImage("mosaic1.png", Show(Mosaic1Pic))
}

func TestMosaic2Pic(*testing.T) {
	saveImage("mosaic2.png", Show(Mosaic2Pic))
}
