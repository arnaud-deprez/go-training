package moretypes

import (
	"fmt"
	"testing"
)

type Vertex struct {
	X int
	Y int
}

func TestStruct(*testing.T) {
	fmt.Println(Vertex{1, 2})
}

func TestStructFieldAccess(*testing.T) {
	v := Vertex{1, 2}
	v.X = 4
	fmt.Println(v)
}

func TestPointerToStruct(*testing.T) {
	v := Vertex{1, 2}
	p := &v
	// shortcut to (*p).X
	p.X = 1e9
	fmt.Println(v)
}

func TestStructLiteral(*testing.T) {
	var (
		v1 = Vertex{1, 2}  // has type Vertex
		v2 = Vertex{X: 1}  // Y:0 is implicit
		v3 = Vertex{}      // X:0 and Y:0
		p  = &Vertex{1, 2} // has type *Vertex
	)
	fmt.Println(v1, p, v2, v3)
}