package moretypes

import (
	"fmt"
	"strings"
	"testing"

	"golang.org/x/tour/wc"
)

type Vertex2 struct {
	Lat, Long float64
}

func TestMap(*testing.T) {
	//The zero value of a map is nil. A nil map has no keys, nor can keys be added.
	var m map[string]Vertex2
	m = make(map[string]Vertex2)
	m["Bell Labs"] = Vertex2{
		40.68433, -74.39967,
	}
	fmt.Println(m["Bell Labs"])
}

func TestMapLiterals(*testing.T) {
	var m = map[string]Vertex2{
		"Bell Labs": Vertex2{
			40.68433, -74.39967,
		},
		"Google": Vertex2{
			37.42202, -122.08408,
		},
	}
	fmt.Println(m)
}

func TestMapLiteralsTypeInference(*testing.T) {
	var m = map[string]Vertex2{
		"Bell Labs": {
			40.68433, -74.39967,
		},
		"Google": {
			37.42202, -122.08408,
		},
	}
	fmt.Println(m)
}

func TestMutatingMap(*testing.T) {
	m := make(map[string]int)

	m["Answer"] = 42
	fmt.Println("The value:", m["Answer"])

	m["Answer"] = 48
	fmt.Println("The value:", m["Answer"])

	delete(m, "Answer")
	fmt.Println("The value:", m["Answer"])

	v, ok := m["Answer"]
	fmt.Println("The value:", v, "Present?", ok)
}

func WordCount(s string) map[string]int {
	words := strings.Fields(s)
	counts := make(map[string]int)
	for _, w := range words {
		counts[w]++
	}
	return counts
}

func TestWorkCount(*testing.T) {
	wc.Test(WordCount)
}
