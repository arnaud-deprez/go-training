package flowcontrol

import (
	"fmt"
	"testing"
)

/**
  * A defer statement defers the execution of a function until the surrounding function returns
  * This can be useful when need to release resources like a try/finally in java.
  * See https://blog.golang.org/defer-panic-and-recover
**/
func TestDefer(*testing.T) {
	// The deferred call's arguments are evaluated immediately,
	// but the function call is not executed until the surrounding function returns.
	defer fmt.Println("world")
	fmt.Println("hello")
	// print hello
	// then print world
}

func TestDeferLiFo(*testing.T) {
	fmt.Println("counting")

	// Print 9 8 7 6...
	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}

	fmt.Println("done")
}
