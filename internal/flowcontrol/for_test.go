package flowcontrol

import (
	"fmt"
	"testing"
)

func TestFor(*testing.T) {
	sum := 0
	// no parenthesis
	for i := 0; i < 10; i++ {
		sum += i
	}
	fmt.Println(sum)
}

func TestForOptionalInitAndPostStatement(*testing.T) {
	sum := 1
	// no parenthesis
	for sum < 1000 {
		sum += sum
	}
	fmt.Println(sum)
}

func TestGoWhile(*testing.T) {
	sum := 1
	for sum < 1000 {
		sum += sum
	}
	fmt.Println(sum)
}

func TestForever(*testing.T) {
	/* for {

	} */
}
