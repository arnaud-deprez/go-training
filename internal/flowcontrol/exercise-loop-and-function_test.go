package flowcontrol

import (
	"fmt"
	"math"
	"testing"
)

func mySqrt(x float64) (z float64, i int) {
	z, dx := 1.0, 1.0
	i = 0
	for ; i < 10 && math.Abs(dx) > 0.00001; i++ {
		dx = (z*z - x) / (2 * z)
		z -= dx
	}
	return
}

func TestSqrt(t *testing.T) {
	v, it := mySqrt(2)
	fmt.Println("Compute: ", v, " ~= ", math.Sqrt(2), " in ", it, " iterations")
}
