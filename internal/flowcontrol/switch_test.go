package flowcontrol

import (
	"fmt"
	"runtime"
	"testing"
	"time"
)

func TestSwitch(*testing.T) {
	fmt.Print("Go runs on ")
	// init in switch
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
		// no need to break
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.", os)
	}
}

func TestSwitchEvaluationOrder(*testing.T) {
	fmt.Println("When's Saturday?")
	today := time.Now().Weekday()
	// case can be string or and does need to be constant
	switch time.Friday {
	case today + 0:
		fmt.Println("Today.")
	case today + 1:
		fmt.Println("Tomorrow.")
	case today + 2:
		fmt.Println("In two days.")
	default:
		fmt.Println("Too far away.")
	}
}

func TestSwitchWithNoCondition(*testing.T) {
	t := time.Now()
	// equivalent to switch true
	// can be a clean way to write long if-then-else
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
}
