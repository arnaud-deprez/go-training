package methods

import (
	"fmt"
	"math"
	"testing"
)

type Abser interface {
	Abs() float64
}

type Vertex2 struct {
	X, Y float64
}

// Go does not have classes but you can define methods on types
// A method is a special function with a receiver argument (here the Vertex named v)
// Value receiver, the method operates on a copy of the original Vertex value
func (v *Vertex2) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func TestInterface(*testing.T) {
	var a Abser
	f := MyFloat(-math.Sqrt2)
	v := Vertex2{3, 4}

	a = f  // a MyFloat implements Abser
	a = &v // a *Vertex implements Abser

	// In the following line, v is a Vertex (not *Vertex)
	// and does NOT implement Abser.
	// a = v

	fmt.Println(a.Abs())
}

type I interface {
	M()
}

type T struct {
	S string
}

// This method means type T implements the interface I,
// but we don't need to explicitly declare that it does so.
func (t *T) M() {
	//In some languages this would trigger a null pointer exception,
	//but in Go it is common to write methods that gracefully handle being called with a nil receiver
	if t == nil {
		fmt.Println("<nil>")
		return
	}
	fmt.Println(t.S)
}

type F float64

func (f F) M() {
	fmt.Println(f)
}

func TestImplicitInterface(*testing.T) {
	i := &T{"hello"}
	i.M()
}

// interface values can be thought as a tuple of a value and a concrete type (value, type)
func TestInterfaceValue(*testing.T) {
	var i I

	i = &T{"hello"}
	describe(i)
	i.M()

	i = F(math.Pi)
	describe(i)
	i.M()
}

func describe(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}

// If the concrete value inside the interface itself is nil, the method will be called with a nil receiver.
func TestInterfaceValueWithNil(*testing.T) {
	var i I

	var t *T
	i = t
	//Note that an interface value that holds a nil concrete value is itself non-nil
	describe(i)
	i.M()

	i = &T{"hello"}
	describe(i)
	i.M()
}

func TestNilInterfaceValue(*testing.T) {
	var i I
	describe(i)
	// A nil interface value holds neither value nor concrete type.
	// Calling a method on a nil interface is a run-time error
	// because there is no type inside the interface tuple to indicate which concrete method to call.
	i.M()
}

func TestEmptyInterface(*testing.T) {
	// Empty interfaces are used by code that handles values of unknown type.
	// For example, fmt.Print takes any number of arguments of type interface{}.
	// A bit like Object in java or Any in scala
	var i interface{}
	describe(i)

	i = 42
	describe(i)

	i = "hello"
	describe(i)
}
