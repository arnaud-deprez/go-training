package methods

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io/ioutil"
	"os"
	"testing"
)

var dirName = "build"

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func TestMain(m *testing.M) {
	er1 := os.MkdirAll("build", os.ModePerm)
	check(er1)
	dir, er2 := ioutil.TempDir("build", "img")
	check(er2)
	dirName = dir
	os.Exit(m.Run())
}
func TestImage(*testing.T) {
	m := image.NewRGBA(image.Rect(0, 0, 100, 100))
	fmt.Println(m.Bounds())
	fmt.Println(m.At(0, 0).RGBA())
}

type MyImage struct {
	Width, Height int
	f             func(x, y int) uint8
}

func (i *MyImage) ColorModel() color.Model {
	return color.RGBAModel
}

func (i *MyImage) Bounds() image.Rectangle {
	return image.Rect(0, 0, i.Width, i.Height)
}

func (i *MyImage) At(x, y int) color.Color {
	return color.RGBA{i.f(x, y), i.f(x, y), 255, 255}
}

func NewImage(f func(x, y int) uint8) MyImage {
	return MyImage {255, 255, f}
}

func saveImage(name string, m image.Image) {
	var buf bytes.Buffer
	err := png.Encode(&buf, m)
	check(err)
	fmt.Printf("Write %s in %s\n", name, dirName)
	ioutil.WriteFile(fmt.Sprintf("%s/%s", dirName, name), buf.Bytes(), 0644)
}

func TestMyImage(*testing.T) {
	m := NewImage(func (x, y int) uint8 { return uint8((x+y)/2) })
	saveImage("median.png", &m)
}
