package methods

import (
	"fmt"
	"testing"
)

func TestTypeAssertions(*testing.T) {
	var i interface{} = "hello"

	s := i.(string)
	fmt.Println(s)

	s, ok := i.(string)
	fmt.Println(s, ok)

	f, ok := i.(float64)
	fmt.Println(f, ok)

	// panic
	// f = i.(float64)
	// fmt.Println(f)
}
