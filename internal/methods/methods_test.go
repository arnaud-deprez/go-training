package methods

import (
	"fmt"
	"math"
	"testing"
)

type Vertex struct {
	X, Y float64
}

// Go does not have classes but you can define methods on types
// A method is a special function with a receiver argument (here the Vertex named v)
// Value receiver, the method operates on a copy of the original Vertex value
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func TestMethod(*testing.T) {
	v := Vertex{3, 4}
	fmt.Println(v.Abs())
}

func Abs(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func TestDifferenceWithFunction(*testing.T) {
	v := Vertex{3, 4}
	fmt.Println(Abs(v))
}

type MyFloat float64

// You can only declare a method with a receiver
// whose type is defined in the same package as the method.
func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func TestMethodOnNonStructType(*testing.T) {
	f := MyFloat(-math.Sqrt2)
	fmt.Println(f.Abs())
}

// Methods with pointer receivers can modify the value to which the receiver points.
func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func TestMethodOnPointer(*testing.T) {
	v := Vertex{3, 4}
	v.Scale(10)
	fmt.Println(v.Abs())
}

func ScaleFunc(v *Vertex, f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

// In general, all methods on a given types should have either value or pointer receivers but a mixture of both.
func TestMethodsAndPointerIndirection(*testing.T) {
	v := Vertex{3, 4}
	v.Scale(2)        // value method call is interpreted as (&v).Scale(2)
	ScaleFunc(&v, 10) // function call needs to be converted to the appropriate type

	p := &Vertex{4, 3}
	p.Scale(3) // pointer method call is syntaxically equivalent to value method call
	ScaleFunc(p, 8)

	fmt.Println(v, p)
}
