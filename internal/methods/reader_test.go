package methods

import (
	"fmt"
	"io"
	"os"
	"strings"
	"testing"

	"golang.org/x/tour/reader"
)

func TestReader(*testing.T) {
	r := strings.NewReader("Hello, Reader!")

	// Consumes 8 bytes at a time
	b := make([]byte, 8)
	for {
		n, err := r.Read(b)
		fmt.Printf("n = %v err = %v b = %v\n", n, err, b)
		fmt.Printf("b[:n] = %q\n", b[:n])
		if err == io.EOF {
			break
		}
	}
}

type MyStreamReader struct {}

func (r MyStreamReader) Read(b []byte) (int, error) {
	for i := range b {
		b[i] = 'A'
	}
	return len(b), nil
}

func TestMyReader(*testing.T) {
	reader.Validate(MyStreamReader{})
}

type rot13Reader struct {
	r io.Reader
}

func (r *rot13Reader) Read(b []byte) (int, error) {
	// call the underlying Reader.Read([]byte)
	l, e := r.r.Read(b)
	for i, c := range b {
		if c >= 'A' && c <= 'Z' {
			b[i] = (c - 'A' + 13) % 26 + 'A'
		} else if c >= 'a' && c <= 'z' {
			b[i] = (c - 'a' + 13) % 26 + 'a'
		}
	}
	return l, e
}

func TestRot13Reader(*testing.T) {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}