package basics

import (
	"fmt"
	"testing"
)

func add(x int, y int) int {
	return x + y
}

//When two or more consecutive named function parameters share a type, you can omit the type from all but the last.
func add2(x, y int) int {
	return x + y
}

func TestAdd(t *testing.T) {
	fmt.Println(add(42, 13))
}

func TestAdd2(t *testing.T) {
	fmt.Println(add2(42, 13))
}

// multiple results
func swap(x, y string) (string, string) {
	return y, x
}

func TestSwap(*testing.T) {
	a, b := swap("hello", "world")
	fmt.Println(a, b)
}

func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	// return named values = return x, y
	return
}

func TestSplit(*testing.T) {
	fmt.Println(split(17))
}