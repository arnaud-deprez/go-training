package basics

import (
	"fmt"
	"testing"
)

func TestVariables(*testing.T) {
	var c, python, java bool
	var i int
	fmt.Println(i, c, python, java)
}

func TestVariablesWithInitializers(*testing.T) {
	var i, j int = 1, 2
	var c, python, java = true, false, "no!"
	fmt.Println(i, j, c, python, java)
}

func TestShortVariablesDeclaration(*testing.T) {
	var i, j int = 1, 2
	k := 3
	c, python, java := true, false, "no!"

	fmt.Println(i, j, k, c, python, java)
}
