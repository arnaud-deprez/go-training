package basics

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"testing"
	"time"
)

// setup/teardown test file
func TestMain(m *testing.M) {
	rand.Seed(time.Now().UTC().UnixNano())
	os.Exit(m.Run())
}

func TestRandom(t *testing.T) {
	fmt.Println("My favorite number is ", rand.Intn(10))
}

func TestExportedName(t *testing.T) {
	// exported name start with capital letter
	fmt.Println(math.Pi)
}
